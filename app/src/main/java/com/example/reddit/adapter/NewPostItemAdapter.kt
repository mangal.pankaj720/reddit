package com.example.reddit.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.reddit.R
import com.example.reddit.db.entity.hotEntity
import com.example.reddit.db.entity.newEntity
import com.example.reddit.model.HotPost

class NewPostItemAdapter(val mCtx: Context, var hotPostList:ArrayList<newEntity>)
    : RecyclerView.Adapter<NewPostItemAdapter.HotPostViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): HotPostViewHolder {
        val view = LayoutInflater.from(mCtx).inflate(R.layout.post_item_layout, parent, false)
        return HotPostViewHolder(view)
    }

    class HotPostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imgPost:ImageView = itemView.findViewById(R.id.imgPost)
        val tvPostTitle:TextView = itemView.findViewById(R.id.tvPostTitle)
        val tvPostDesc:TextView = itemView.findViewById(R.id.tvPostDesc)
        val tvPostReddit:TextView = itemView.findViewById(R.id.tvPostReddit)
    }

    override fun getItemCount(): Int {
       return hotPostList.size
    }

    fun filterList(filterList: ArrayList<newEntity>) {
        hotPostList = filterList
        notifyDataSetChanged()
    }

    fun addItems(items : ArrayList<newEntity>) {
        hotPostList.addAll(items)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: NewPostItemAdapter.HotPostViewHolder, position: Int) {
        val hotPost = hotPostList[position]
        holder.tvPostTitle.text = hotPost.title
        holder.tvPostReddit.text = hotPost.subReddit

        /*Picasso.get()
            .load(hotPost.dataCH?.all_awardings!![0].icon_url)
            .into(holder.imgPost)*/
    }
}