package com.example.reddit.adapter

import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.reddit.view.frag.HotFrag
import com.example.reddit.view.frag.NewFrag

class ViewPagerFragAdapter(val fm:FragmentManager, val countFrag:Int,
                           edtSearchQuery:EditText) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {

        var frag:Fragment ?= null
        when(position){
            0 -> {
                frag = HotFrag()
                return frag
            }

            1 -> {
                frag = NewFrag()
                return frag
            }

            else -> {return frag?: Fragment()
            }
        }
    }

    override fun getCount(): Int {
        return countFrag
    }

    override fun getPageTitle(position: Int): CharSequence? {
//        return super.getPageTitle(position)

        when(position){

            0 -> return "HOT"
            1 -> return "NEW"
            else -> return ""
        }
    }
}