package com.example.reddit.util

import android.util.Log
import com.example.reddit.db.entity.newEntity

class PaginatorClass(var walletUserDataList:ArrayList<newEntity>) {

    var TOTAL_NUM_ITEMS = walletUserDataList.size
    val ITEMS_PER_PAGE = 5
    val ITEMS_REMAINING = TOTAL_NUM_ITEMS % ITEMS_PER_PAGE
    val LAST_PAGE = TOTAL_NUM_ITEMS / ITEMS_PER_PAGE

    init {
        Log.e("paginator :: ", "Total Items :: $TOTAL_NUM_ITEMS \n Items per page :: $ITEMS_PER_PAGE \n" +
                "Remaining Items :: $ITEMS_REMAINING \n Total/Last page :: $LAST_PAGE")
    }

    fun generatePage(currentPage: Int): ArrayList<newEntity>? {
        /* val startItem = currentPage * ITEMS_PER_PAGE + 1*/
        val startItem = currentPage * ITEMS_PER_PAGE
        val numOfData = ITEMS_PER_PAGE

        Log.e("generatePage", " :: $currentPage :: $startItem :: $numOfData")
        val pageData: ArrayList<newEntity> = ArrayList()

        if(TOTAL_NUM_ITEMS<=startItem){

        }else{
            if (currentPage == LAST_PAGE && ITEMS_REMAINING > 0) {
                for (i in startItem until startItem + ITEMS_REMAINING) {
                    pageData.add(walletUserDataList[i])
                }
            } else {
                for (i in startItem until startItem + numOfData) {
                    pageData.add(walletUserDataList[i])
                }
            }
        }
        return pageData
    }
}