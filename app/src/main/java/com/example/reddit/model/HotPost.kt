package com.example.reddit.model

import com.google.gson.annotations.SerializedName

data class HotPost(
    var kind: String? = null,
    @SerializedName("data")
    var data: MainData? = null) {

    data class MainData(
        var children: ArrayList<ChildrenList>? = null
    ) {
        data class ChildrenList(
            @SerializedName("data")
            var dataCH: ChildrenMainData? = null
        ) {
            data class ChildrenMainData(
                var title: String? = null,
                var subreddit:String ?= null,
                var all_awardings: ArrayList<Awardings>? = null
            ) {
                data class Awardings(
                    var description: String? = null,
                    var icon_url: String? = null
                )
            }
        }
    }
}