package com.example.reddit.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices = [Index(value = ["titleNew"], unique = true)])
class newEntity {

    @PrimaryKey(autoGenerate = true)
    var id = 0

    @ColumnInfo(name = "titleNew")
    var title: String? = null

    @ColumnInfo(name = "subRedditNew")
    var subReddit: String? = null
}