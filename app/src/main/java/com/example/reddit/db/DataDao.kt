package com.example.reddit.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.reddit.db.entity.hotEntity
import com.example.reddit.db.entity.newEntity

@Dao
interface DataDao {

    @Query("SELECT * FROM hotEntity")
    suspend fun getAllHot(): List<hotEntity>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllHot(post: List<hotEntity>)

    @Insert
    suspend fun insert(task: hotEntity?)

    @Query("DELETE FROM hotEntity")
    suspend fun hotEntityClear()

    @Query("SELECT COUNT(*) FROM hotEntity")
    fun getCount(): LiveData<Int>?

    /*@Delete
    fun delete(task: hotEntity?)

    @Update
    fun update(task: hotEntity?)*/

    @Query("SELECT * FROM newEntity")
    suspend fun getAllNew(): List<newEntity>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllNewPost(post: List<newEntity>)

    @Query("DELETE FROM newEntity")
    suspend fun newEntityClear()

    @Insert
    suspend fun insertNew(task: newEntity?)
}