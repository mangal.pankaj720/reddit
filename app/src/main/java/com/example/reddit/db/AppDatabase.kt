package com.example.reddit.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.reddit.db.entity.hotEntity
import com.example.reddit.db.entity.newEntity

@Database(entities = arrayOf(hotEntity::class, newEntity::class), version = 1, exportSchema = false)
abstract class AppDatabase:RoomDatabase() {
    abstract fun getDao():DataDao

    companion object{
        @Volatile private var instance:AppDatabase ?= null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK){
            instance?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(context.applicationContext,
            AppDatabase::class.java,
            "redditDB")
            .fallbackToDestructiveMigration()
            .build()
    }
}