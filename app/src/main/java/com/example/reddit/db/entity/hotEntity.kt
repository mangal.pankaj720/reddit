package com.example.reddit.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices = [Index(value = ["titleHot"], unique = true)])
class hotEntity {

    @PrimaryKey(autoGenerate = true)
    var id = 0

    @ColumnInfo(name = "titleHot")
    var title: String? = null

    @ColumnInfo(name = "subRedditHot")
    var subReddit: String? = null
}