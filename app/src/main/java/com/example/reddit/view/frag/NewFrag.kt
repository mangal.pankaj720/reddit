package com.example.reddit.view.frag

import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.reddit.R
import com.example.reddit.adapter.HotPostItemAdapter
import com.example.reddit.adapter.NewPostItemAdapter
import com.example.reddit.data.api.apiInterface
import com.example.reddit.data.communicator.NetworkCommunicator
import com.example.reddit.db.AppDatabase
import com.example.reddit.db.entity.hotEntity
import com.example.reddit.db.entity.newEntity
import com.example.reddit.model.HotPost
import com.example.reddit.util.PaginatorClass
import com.example.reddit.view.MainActivity
import com.example.reddit.view.TextWatcherListener
import com.example.reddit.view.base.BaseFragment
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewFrag : BaseFragment(), TextWatcherListener {

    internal lateinit var view:View
    private lateinit var rvNewPost:RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var hotPostItemAdapter: NewPostItemAdapter
    private var newList:List<newEntity> ?= null
    private lateinit var pbNewPost:ProgressBar

    private var pageN0 = 0
    private var totalItem = 8
    private var isLoading = true
    private var pastVisibleItems:Int ?= null
    private var visibleItemCount:Int ?= null
    private var totalItemCount:Int ?= null
    private var tempTotalUserList:ArrayList<newEntity> ?= null
    private lateinit var paginatorClassObject: PaginatorClass

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        view = inflater.inflate(R.layout.fragment_new, container, false)
        setupLayoutManager()
//        getNewPostData()

        (context as MainActivity).setData(this)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        launch {
            context?.let {
                newList = AppDatabase(it).getDao().getAllNew()
                Log.e("hotlist", " :: ${newList!!.size}")
                paginatorClassObject = PaginatorClass(newList as ArrayList<newEntity>)
                tempTotalUserList = paginatorClassObject.generatePage(pageN0)
                hotPostItemAdapter = NewPostItemAdapter(context!!, tempTotalUserList as ArrayList<newEntity>)
                rvNewPost.adapter = hotPostItemAdapter
            }
        }

         rvNewPost.addOnScrollListener(object : RecyclerView.OnScrollListener(){

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                visibleItemCount = (layoutManager as LinearLayoutManager).childCount
                totalItemCount = (layoutManager as LinearLayoutManager).itemCount
                pastVisibleItems = (layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

                Log.e("scroll value ", " :: $visibleItemCount :: $totalItemCount :: $pastVisibleItems")

                    if(dy > 0){
                        if(isLoading && (visibleItemCount!!+pastVisibleItems!!) >= totalItemCount!!){
                            pageN0++
                            //performPagination
                            tempTotalUserList = ArrayList()
                            isLoading = false
                            pbNewPost.visibility = View.VISIBLE

                            val handler = Handler()
                            handler.postDelayed({
                                val newItems = paginatorClassObject.generatePage(pageN0)
                                hotPostItemAdapter.addItems(newItems!!)
                                isLoading = true

                                pbNewPost.visibility = View.GONE
                            }, 1000)
                        }
                    }
            }
        })
    }

    private fun setupLayoutManager(){
        pbNewPost = view.findViewById(R.id.pbNewPost)
        rvNewPost = view.findViewById(R.id.rvNewPost)
        layoutManager = LinearLayoutManager(context)
        rvNewPost.layoutManager = layoutManager
        rvNewPost.itemAnimator = DefaultItemAnimator()
        rvNewPost.hasFixedSize()
    }

    /*private lateinit var totalItem:ArrayList<HotPost.MainData.ChildrenList>
    private fun getNewPostData() {

        val call: Call<HotPost> = NetworkCommunicator.instance.createApiClient(apiInterface::class.java).getNewPost()
        call.enqueue(object : Callback<HotPost> {
            override fun onFailure(call: Call<HotPost>, t: Throwable) {
                Toast.makeText(context,"Please check your Internet " +
                        "Connection", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<HotPost>, response: Response<HotPost>) {

                val resp = response.body()
                Log.e("new post", " :: ${resp?.data?.children?.size}")
                Log.e("hot post", " :: ${resp?.data?.children?.size}")

                totalItem = ArrayList()
                totalItem = resp?.data?.children!!
                hotPostItemAdapter = HotPostItemAdapter(context!!, totalItem)
                rvNewPost.adapter = hotPostItemAdapter
            }
        })
    }*/

    override fun onTextChanged(text: String) {
        Log.e("value", " :: ${text}")
        filterWalletUser(text)
    }

    private var filteredList:ArrayList<newEntity> ?= null
    private fun filterWalletUser(filterText: String) {
        Log.e("new filterText", " :: ${filterText}")
        filteredList = ArrayList()
        for (op in newList!!.indices){
            if(newList!![op].subReddit!!.toLowerCase().contains(filterText.toLowerCase())){
                filteredList?.add(newList!![op])
            }
        }

        Log.e("new filterText", " :: ${filteredList?.size}")
        hotPostItemAdapter.filterList(filteredList?: ArrayList())
//        walletUserAdapter = WalletUserRv(this@WalletUserPage, filteredList?:ArrayList())
//        rvFilterDataShow.adapter = walletUserAdapter
    }
}
