package com.example.reddit.view

interface HotTextWatcherListener {
    fun onTextChanged(text:String)
}