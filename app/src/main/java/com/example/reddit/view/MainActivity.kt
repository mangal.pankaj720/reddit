package com.example.reddit.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import com.example.reddit.R
import com.example.reddit.adapter.ViewPagerFragAdapter
import com.example.reddit.data.api.apiInterface
import com.example.reddit.data.communicator.NetworkCommunicator
import com.example.reddit.db.AppDatabase
import com.example.reddit.db.entity.hotEntity
import com.example.reddit.db.entity.newEntity
import com.example.reddit.model.HotPost
import com.example.reddit.util.inputStreamToString
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(), ViewPager.OnPageChangeListener,
    TabLayout.OnTabSelectedListener {

    private lateinit var tblCategory: TabLayout
    private lateinit var vpRetailShopCategory: ViewPager
    private lateinit var edtSearchQuery:EditText
    private var textWatcherListener:TextWatcherListener ?= null
    private var hotTextWatcherListener:HotTextWatcherListener ?= null

    private lateinit var hotEntityList:List<hotEntity>
    private lateinit var totalItem:ArrayList<HotPost.MainData.ChildrenList>

    private lateinit var newEntityList:List<newEntity>
    private lateinit var newtotalItem:ArrayList<HotPost.MainData.ChildrenList>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()

        hotEntityList = ArrayList()
        totalItem = ArrayList()
        newEntityList = ArrayList()
        newtotalItem = ArrayList()

        val pagerAdapter = ViewPagerFragAdapter(supportFragmentManager, tblCategory.tabCount, edtSearchQuery)
        vpRetailShopCategory.adapter = pagerAdapter

        edtSearchQuery.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(vpRetailShopCategory.currentItem == 0){
                    hotTextWatcherListener?.onTextChanged(s.toString())
                }else  if(vpRetailShopCategory.currentItem == 1){
                    textWatcherListener?.onTextChanged(s.toString())
                }

            }
        })

        /*CoroutineScope(Dispatchers.Main+ Job()).launch {
            this@MainActivity.let {
                AppDatabase(it).getDao().hotEntityClear()
            }
        }

        CoroutineScope(Dispatchers.Main+ Job()).launch {
            this@MainActivity.let {
                AppDatabase(it).getDao().newEntityClear()
            }
        }*/

        val hotJson = inputStreamToString(resources.openRawResource(R.raw.hot_post))
        val newJson = inputStreamToString(resources.openRawResource(R.raw.new_post))
        val newPostModel = Gson().fromJson(newJson, HotPost::class.java)
        val myModel = Gson().fromJson(hotJson, HotPost::class.java)
        Log.e("mainActivity hot", " :: ${myModel.data?.children?.size}")
        Log.e("mainActivity new", " :: ${newPostModel.data?.children?.size}")

        totalItem = myModel?.data?.children!!
        for (i in 0 until totalItem.size){
            val hotEntity = hotEntity()
            hotEntity.title = totalItem[i].dataCH?.title
            hotEntity.subReddit = totalItem[i].dataCH?.subreddit
            (hotEntityList as ArrayList<hotEntity>).add(hotEntity)
        }

        newtotalItem = newPostModel?.data?.children!!
        for (i in 0 until newtotalItem.size){
            val newEntity = newEntity()
            newEntity.title = newtotalItem[i].dataCH?.title
            newEntity.subReddit = newtotalItem[i].dataCH?.subreddit
            (newEntityList as ArrayList<newEntity>).add(newEntity)
        }

        CoroutineScope(Dispatchers.Main + Job()).launch {
            this@MainActivity.let {
                AppDatabase(it).getDao().insertAllHot(hotEntityList)
            }
        }

        CoroutineScope(Dispatchers.Main+ Job()).launch {
            this@MainActivity.let {
                AppDatabase(it).getDao().insertAllNewPost(newEntityList)
            }
        }

        vpRetailShopCategory.addOnPageChangeListener(this)
        tblCategory.addOnTabSelectedListener(this)
    }

    fun setData(textWatcherListener:TextWatcherListener ?){
        this.textWatcherListener = textWatcherListener
    }

    fun setHotData(hotTextWatcherListener:HotTextWatcherListener ?){
        this.hotTextWatcherListener = hotTextWatcherListener
    }

    private fun initViews(){
        tblCategory = findViewById(R.id.tblCategory)
        vpRetailShopCategory = findViewById(R.id.vpRetailShopCategory)
        edtSearchQuery = findViewById(R.id.edtSearchQuery)

        tblCategory.addTab(tblCategory.newTab().setText("HOT"))
        tblCategory.addTab(tblCategory.newTab().setText("NEW"))
        tblCategory.setupWithViewPager(vpRetailShopCategory)
//        tblCategory.tabGravity = TabLayout.GRAVITY_FILL
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        tblCategory.selectedTabPosition
    }

    override fun onTabReselected(tab: TabLayout.Tab?) {

    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {

    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        vpRetailShopCategory.setCurrentItem(tab!!.position, true)
    }
}
