package com.example.reddit.view

interface TextWatcherListener {
    fun onTextChanged(text:String)
}