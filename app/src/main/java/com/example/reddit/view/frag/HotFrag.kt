package com.example.reddit.view.frag

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.reddit.R
import com.example.reddit.adapter.HotPostItemAdapter
import com.example.reddit.data.api.apiInterface
import com.example.reddit.data.communicator.NetworkCommunicator
import com.example.reddit.db.AppDatabase
import com.example.reddit.db.entity.hotEntity
import com.example.reddit.model.HotPost
import com.example.reddit.util.NetworkUtils
import com.example.reddit.view.HotTextWatcherListener
import com.example.reddit.view.MainActivity
import com.example.reddit.view.TextWatcherListener
import com.example.reddit.view.base.BaseFragment
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HotFrag : BaseFragment(), HotTextWatcherListener {

    internal lateinit var view:View
    private lateinit var rvHotPost: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var hotPostItemAdapter: HotPostItemAdapter
    private var hotList:List<hotEntity> ?= null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        view = inflater.inflate(R.layout.fragment_hot, container, false)
        setupLayoutManager()

        (context as MainActivity).setHotData(this)
        /*launch {
            context?.let {
                val data: LiveData<Int>? = AppDatabase(it).getDao().getCount()
                activity?.let { it1 ->
                    data?.observe(
                        it1,
                        androidx.lifecycle.Observer<Int> { t -> Log.e("survey db count", " :: $t") })
                }
            }
        }*/
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        launch {
            context?.let {
                hotList = AppDatabase(it).getDao().getAllHot()
                Log.e("hotlist", " :: ${hotList!!.size}")
                hotPostItemAdapter = HotPostItemAdapter(context!!, hotList as ArrayList<hotEntity>)
                rvHotPost.adapter = hotPostItemAdapter
            }
        }
    }

    private fun setupLayoutManager(){
        rvHotPost = view.findViewById(R.id.rvHotPost)
        layoutManager = LinearLayoutManager(context)
        rvHotPost.layoutManager = layoutManager
        rvHotPost.itemAnimator = DefaultItemAnimator()
        rvHotPost.hasFixedSize()
    }

   /* private lateinit var hotEntityList:List<hotEntity>
    private lateinit var totalItem:ArrayList<HotPost.MainData.ChildrenList>
    private fun getHotPostData() {
        val call: Call<HotPost> = NetworkCommunicator.instance.createApiClient(apiInterface::class.java).getHotPost()

        call.enqueue(object : Callback<HotPost> {
            override fun onFailure(call: Call<HotPost>, t: Throwable) {
                Toast.makeText(context,"Please check your Internet " +
                        "Connection", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<HotPost>, response: Response<HotPost>) {
                val resp = response.body()
                Log.e("hot post", " :: ${resp?.data?.children?.size}")

                hotEntityList = ArrayList()
                totalItem = ArrayList()
                totalItem = resp?.data?.children!!

                for (i in 0 until totalItem.size){
                    val hotEntity = hotEntity()
                    hotEntity.title = totalItem[i].dataCH?.title
                    hotEntity.subReddit = totalItem[i].dataCH?.subreddit
                    (hotEntityList as ArrayList<hotEntity>).add(hotEntity)
                }

                launch {
                    context?.let {
                        AppDatabase(it).getDao().insertAllHot(hotEntityList)
                    }
                }
                hotPostItemAdapter = HotPostItemAdapter(context!!, totalItem)
                rvHotPost.adapter = hotPostItemAdapter
            }
        })
    }*/

    override fun onTextChanged(text: String) {
        Log.e("value", " :: $text")
        filterWalletUser(text)
    }

    private var filteredList:ArrayList<hotEntity> ?= null
    private fun filterWalletUser(filterText: String) {
        Log.e("hot filterText", " :: ${filterText}")
        filteredList = ArrayList()
        for (op in hotList!!.indices){
            if(hotList!![op].subReddit!!.toLowerCase().contains(filterText.toLowerCase())){
                filteredList?.add(hotList!![op])
            }
        }


        Log.e("hot filterText", " :: ${filteredList?.size}")
        hotPostItemAdapter.filterList(filteredList?: ArrayList())
        /*hotPostItemAdapter = HotPostItemAdapter(context!!, filteredList?:ArrayList())
        rvHotPost.adapter = hotPostItemAdapter
        hotPostItemAdapter.notifyDataSetChanged()*/
//        walletUserAdapter = WalletUserRv(this@WalletUserPage, filteredList?:ArrayList())
//        rvFilterDataShow.adapter = walletUserAdapter
    }
}
