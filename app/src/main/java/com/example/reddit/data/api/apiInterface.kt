package com.example.reddit.data.api

import com.example.reddit.model.HotPost
import retrofit2.Call
import retrofit2.http.GET

interface apiInterface {

    @GET("hot.json")
    fun getHotPost(): Call<HotPost>

    @GET("new.json")
    fun getNewPost(): Call<HotPost>
}