package com.example.reddit.data.communicator

import com.example.reddit.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkCommunicator {

    companion object {
        val instance = NetworkCommunicator()
    }

    /**
     * @param clazz Api interface reference
     * @param <T>   Wild card object type
     * @return Type of Api provider class to be return
    </T> */

    fun <T> createApiClient(clazz: Class<T>): T {

        val logging= HttpLoggingInterceptor()
        logging.level= HttpLoggingInterceptor.Level.BODY
        val  httpclient= OkHttpClient.Builder()
        httpclient.addInterceptor{chain->
            val original=chain.request()
            val  request=original.newBuilder()
//                request.addHeader("accept","application/json")
            val request1=request.build()
            chain.proceed(request1)
        }
        httpclient.connectTimeout(60,java.util.concurrent.TimeUnit.SECONDS)
        httpclient.readTimeout(60,java.util.concurrent.TimeUnit.SECONDS)
        httpclient.addInterceptor(logging)

        /*.addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))*/
        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpclient.build())
            .build()
        return retrofit.create(clazz)
    }
}